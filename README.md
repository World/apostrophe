[![Please do not theme this app](https://stopthemingmy.app/badge.svg)](https://stopthemingmy.app)

# Apostrophe

![](screenshots/main.png)

## About

Apostrophe is a [GTK+](https://www.gtk.org) based distraction free Markdown editor, originally developed by Wolf Vollprecht and currently developed and maintained by Manuel Genovés. It uses pandoc as back-end for parsing Markdown and offers a very clean and sleek user interface.

## Install

<a href='https://flathub.org/apps/details/org.gnome.gitlab.somas.Apostrophe'><img width='240' alt='Download on Flathub' src='https://flathub.org/api/badge?svg&locale=en'/></a>

Also several unofficial builds are available:

* Nix(OS): [`pkgs.apostrophe`](https://github.com/NixOS/nixpkgs/blob/master/pkgs/applications/editors/apostrophe/default.nix)
* [Fedora](https://src.fedoraproject.org/rpms/apostrophe): `sudo dnf install apostrophe`

## Translation

If you want to help translating the project, please join us at [Damned Lies](https://l10n.gnome.org/module/apostrophe/).
Any help is appreciated!

## Building

### Building using GNOME Builder

GNOME Builder offers the easiest method to build Apostrophe. Just follow [this guide](https://welcome.gnome.org/app/Apostrophe/#getting-the-app-to-build) and you'll be up and running in a minute.

### Building from Git

To build Apostrophe from source you need to have the following dependencies installed:

- Build system: `meson ninja-build`
- Pandoc, the program used to convert Markdown to basically anything else: `pandoc`
- GTK4 and GLib development packages: `libgtk-4-dev libglib2.0-dev`
- Rendering the preview panel: `libwebkit2gtk`

- Python dependencies: `python3 python3-regex python3-setuptools python3-levenshtein python3-enchant python3-gi python3-cairo python3-pypandoc`
- A copy of reveal.js in the apropriate directory. Flatpak takes care of it for you, packagers should put it into prefix/share/apostrophe/libs/reveal.js, fully unzipped
- Libspelling and sourceview.
- FiraSans-Regular, FiraMono-Regular, FiraMono-Bold, FiraMono-Medium
- *optional:* AppStream utility: `appstreamcli`
- *optional:* pdftex module: `texlive texlive-latex-extra`

Depending on your setup you may need to install these schemas before building:

```bash
$ sudo cp data/org.gnome.gitlab.somas.Apostrophe.gschema.xml /usr/share/glib-2.0/schemas/org.gnome.gitlab.somas.Apostrophe.gschema.xml
$ sudo glib-compile-schemas /usr/share/glib-2.0/schemas
```

Once all dependencies are installed you can build Apostrophe using the following commands:

```bash
$ git clone https://gitlab.gnome.org/World/apostrophe/
$ cd apostrophe
$ meson builddir --prefix=/usr -Dprofile=development
$ sudo ninja -C builddir install
```

Then you can run the installed package:

```bash
$ apostrophe
```

Or a local version which runs from the source tree
```bash
$ ./builddir/local-apostrophe
```



### Building a flatpak package

It's also possible to build, run and debug a flatpak package. All you need is to setup [flatpak-builder](https://docs.flatpak.org/en/latest/first-build.html) and run the following commands:

```bash
$ cd build-aux/flatpak
$ flatpak-builder --force-clean --install --user _build org.gnome.gitlab.somas.Apostrophe.json
```
