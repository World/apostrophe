# Indonesian translation for apostrophe.
# Copyright (C) 2021 apostrophe's COPYRIGHT HOLDER
# This file is distributed under the same license as the apostrophe package.
# Kukuh Syafaat <kukuhsyafaat@gnome.org>, 2021-2024
#
msgid ""
msgstr ""
"Project-Id-Version: apostrophe main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/apostrophe/issues\n"
"POT-Creation-Date: 2024-03-03 20:09+0000\n"
"PO-Revision-Date: 2024-03-07 16:51+0700\n"
"Last-Translator: Kukuh Syafaat <kukuhsyafaat@gnome.org>\n"
"Language-Team: Indonesian <gnome-l10n-id@googlegroups.com>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 3.4.2\n"

#: data/org.gnome.gitlab.somas.Apostrophe.desktop.in.in:3
#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in:4
msgid "Apostrophe"
msgstr "Apostrophe"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! Do NOT translate "uberwriter" or "apostrophe"! The list MUST also end with a semicolon!
#: data/org.gnome.gitlab.somas.Apostrophe.desktop.in.in:5
msgid "uberwriter;UberWriter;apostrophe;markdown;editor;text;write;"
msgstr ""
"uberwriter;UberWriter;apostrophe;markdown;penyunting;teks;menulis;editor;"

#: data/org.gnome.gitlab.somas.Apostrophe.desktop.in.in:6
msgid "Apostrophe, an elegant, distraction-free markdown editor"
msgstr "Apostrophe, penyunting markdown yang elegan dan bebas gangguan"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:29
msgid "Color scheme"
msgstr "Skema warna"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:30
msgid "Use the color scheme in the application's UI and in the text area."
msgstr "Gunakan skema warna di UI aplikasi dan di area teks."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:36
msgid "Check spelling while typing"
msgstr "Memeriksa ejaan saat mengetik"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:37
msgid "Enable or disable spellchecking."
msgstr "Aktifkan atau nonaktifkan pemeriksaan ejaan."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:43
msgid "Synchronize editor/preview scrolling"
msgstr "Sinkronkan penyunting/pratinjau menggulir"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:44
msgid "Keep the editor and preview scroll positions in sync."
msgstr "Jaga agar penyunting dan pratinjau gulir dalam posisi sinkron."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:50
msgid "Input format"
msgstr "Format masukan"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:51
msgid "Input format to use when previewing and exporting using Pandoc."
msgstr ""
"Format masukan yang akan digunakan saat mempratinjau dan diekspor "
"menggunakan Pandoc."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:57
msgid "Autohide Headerbar"
msgstr "Sembunyi Otomatis Bilah Tajuk"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:58
msgid "Hide the header and status bars when typing."
msgstr "Sembunyikan bilah tajuk dan status saat mengetik."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:64
msgid "Open file base path"
msgstr "Buka base path berkas"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:65
msgid "Open file paths of the current session"
msgstr "Membuka path berkas dari sesi saat ini"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:71
msgid "Default statistic"
msgstr "Statistik bawaan"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:72
msgid "Which statistic is shown on the main window."
msgstr "Statistik mana yang ditampilkan di jendela utama."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:78
msgid "Characters per line"
msgstr "Karakter per baris"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:79
msgid "Maximum number of characters per line within the editor."
msgstr "Cacah maksimum karakter per baris dalam penyunting."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:85
#: data/ui/AboutHemingway.ui:6
msgid "Hemingway Mode"
msgstr "Mode Hemingway"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:86
msgid "Whether the user can delete text or not."
msgstr "Apakah pengguna dapat menghapus teks atau tidak."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:92
msgid "Hemingway Toast Count"
msgstr "Cacah Hemingway Toast"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:93
msgid "Number of times the Hemingway Toast has been shown"
msgstr "Berapa kali Hemingway Toast telah ditampilkan"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:99
msgid "Preview mode"
msgstr "Mode pratinjau"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:100
msgid "How to display the preview."
msgstr "Cara menampilkan pratinjau."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:106
msgid "Preview active"
msgstr "Pratinjau aktif"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:107
msgid "Whether showing of preview is active when launching a new window."
msgstr ""
"Apakah menampilkan pratinjau aktif saat meluncurkan suatu jendela baru."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:113
msgid "Text size"
msgstr "Ukuran teks"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:114
msgid "Preferred relative size for the text."
msgstr "Ukuran relatif yang disukai bagi teks."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:120
msgid "Toolbar"
msgstr "Bilah Alat"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:121
msgid "Whether the toolbar is shown or not."
msgstr "Apakah bilah alat ditampilkan atau tidak."

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in:5
msgid "Edit Markdown in style"
msgstr "Sunting Markdown dalam gaya"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in:14
msgid "Focus on your writing with a clean, distraction-free markdown editor."
msgstr ""
"Fokus pada tulisan Anda dengan penyunting markdown yang bersih dan bebas "
"gangguan."

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in:15
msgid "Features:"
msgstr "Fitur:"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in:17
msgid "An UI tailored to comfortable writing"
msgstr "UI yang disesuaikan dengan tulisan yang nyaman"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in:18
msgid "A distraction-free mode"
msgstr "Mode bebas gangguan"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in:19
msgid "Dark, light and sepia themes"
msgstr "Tema gelap, terang dan sepia"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in:20
msgid ""
"Everything you expect from a text editor, such as spellchecking or document "
"statistics"
msgstr ""
"Semua yang Anda harapkan dari penyunting teks, seperti pemeriksaan ejaan "
"atau statistik dokumen"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in:21
msgid "Live preview of what you write"
msgstr "Pratinjau langsung dari apa yang Anda tulis"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in:22
msgid ""
"Export to all kind of formats: PDF, Word/Libreoffice, LaTeX, or even HTML "
"slideshows"
msgstr ""
"Ekspor ke semua jenis format: PDF, Word/Libreoffice, LaTeX, atau bahkan "
"tayangan salindia HTML"

#: data/ui/About.ui.in:9
msgid "Copyright (C) 2022, Manuel G., Wolf V."
msgstr "Hak Cipta (C) 2020, Manuel G., Wolf V."

#. Put your name in here, like this:
#. Manuel Genovés <manuel.genoves@gmail.com>
#: data/ui/About.ui.in:17
msgid "translator-credits"
msgstr ""
"Kukuh Syafaat <kukuhsyafaat@gnome.org>, 2021-2024.\n"
"Andika Triwidada <andika@gmail.com>, 2022."

#: data/ui/AboutHemingway.ui:7
msgid ""
"The Hemingway Mode mimics the experience of writing on a typewriter, not "
"allowing you to delete any text or doing any kind of editing. \n"
"\n"
"Only being able to move forward may be frustrating, but can be an "
"interesting exercise for improving your writing skills, gaining focus or "
"facing the task on hand with a different mindset.\n"
"  "
msgstr ""
"Mode Hemingway meniru pengalaman menulis di mesin tik, tidak memungkinkan "
"Anda untuk menghapus teks apa pun atau melakukan penyuntingan apa pun. \n"
"\n"
"Hanya mampu bergerak maju mungkin membuat frustrasi, tetapi bisa menjadi "
"latihan yang menarik untuk meningkatkan keterampilan menulis Anda, "
"mendapatkan fokus atau menghadapi tugas yang ada dengan pola pikir yang "
"berbeda.\n"
"  "

#: data/ui/AboutHemingway.ui:13
msgid "_Close"
msgstr "_Tutup"

#: data/ui/Export.ui:33 apostrophe/export_dialog.py:131
#: apostrophe/export_dialog.py:342 apostrophe/export_dialog.py:347
#: apostrophe/main_window.py:439 apostrophe/main_window.py:513
#: apostrophe/main_window.py:605
msgid "Cancel"
msgstr "Batal"

#: data/ui/Export.ui:44 apostrophe/export_dialog.py:126
#: apostrophe/export_dialog.py:341 apostrophe/export_dialog.py:345
msgid "Export"
msgstr "Ekspor"

#: data/ui/Export.ui:117
msgid "Options"
msgstr "Opsi"

#: data/ui/Export.ui:121
msgid "Standalone"
msgstr "Mandiri"

#: data/ui/Export.ui:123
msgid ""
"Use a header and footer to include things like stylesheets and meta "
"information"
msgstr ""
"Gunakan tajuk dan kaki untuk menyertakan hal-hal seperti stylesheet dan "
"informasi meta"

#: data/ui/Export.ui:136
msgid "Table of Contents"
msgstr "Daftar isi"

#: data/ui/Export.ui:150
msgid "Number Sections"
msgstr "Bagian Angka"

#: data/ui/Export.ui:168 data/ui/Export.ui:173 data/ui/Export.ui:185
#: data/ui/Export.ui:190
msgid "Page Size"
msgstr "Ukuran Halaman"

#: data/ui/Export.ui:202
msgid "HTML Options"
msgstr "Opsi HTML"

#: data/ui/Export.ui:207
msgid "Self-Contained"
msgstr "Mandiri"

#: data/ui/Export.ui:209
msgid "Produces an HTML file with no external dependencies"
msgstr "Hasilkan berkas HTML tanpa dependensi eksternal"

#: data/ui/Export.ui:225
msgid "Syntax Highlighting"
msgstr "Penyorotan Sintaks"

#: data/ui/Export.ui:231
msgid "Use Syntax Highlighting"
msgstr "Gunakan Penyorotan Sintaks"

#: data/ui/Export.ui:238
msgid "Highlight Style"
msgstr "Gaya Sorot"

#: data/ui/Export.ui:252
msgid "Presentation"
msgstr "Presentasi"

#: data/ui/Export.ui:257
msgid "Incremental Bullets"
msgstr "Bulatan Inkremental"

#: data/ui/Export.ui:259
msgid "Show one bullet point after another in a slideshow"
msgstr "Tampilkan satu poin demi satu dalam tayangan salindia"

#: data/ui/Headerbar.ui:11
msgid "New"
msgstr "Baru"

#: data/ui/Headerbar.ui:18
msgid "_Open"
msgstr "_Buka"

#: data/ui/Headerbar.ui:20
msgid "Open a Markdown File"
msgstr "Buka Berkas Markdown"

#: data/ui/Headerbar.ui:26
msgid "_Save"
msgstr "_Simpan"

#: data/ui/Headerbar.ui:28
msgid "Save the Markdown File"
msgstr "Simpan Berkas Markdown"

#: data/ui/Headerbar.ui:44
msgid "Open the Main Menu"
msgstr "Buka Menu Utama"

#: data/ui/Headerbar.ui:55
msgid "_Find"
msgstr "_Cari"

#: data/ui/Headerbar.ui:57
msgid "Search in the File"
msgstr "Cari di Berkas"

#: data/ui/Headerbar.ui:80
msgid "_Hemingway Mode"
msgstr "Mode _Hemingway"

#: data/ui/Headerbar.ui:84
msgid "_Focus Mode"
msgstr "Mode _Fokus"

#: data/ui/Headerbar.ui:90
msgid "Find and _Replace"
msgstr "Ca_ri dan Ganti"

#: data/ui/Headerbar.ui:96
msgid "_Preferences"
msgstr "_Preferensi"

#: data/ui/Headerbar.ui:102
msgid "Open _Tutorial"
msgstr "Buka _Tutorial"

#: data/ui/Headerbar.ui:107
msgid "_Keyboard Shortcuts"
msgstr "Pintasan Papan Ti_k"

#: data/ui/Headerbar.ui:111
msgid "_About Apostrophe"
msgstr "Tent_ang Apostrophe"

#: data/ui/Headerbar.ui:122
msgid "_Save As..."
msgstr "_Simpan Sebagai…"

#: data/ui/Headerbar.ui:145
msgid "Advanced _Export…"
msgstr "_Ekspor Lanjutan…"

#: data/ui/Headerbar.ui:151
msgid "_Copy HTML"
msgstr "Sa_lin HTML"

#: data/ui/Preferences.ui:15 data/ui/Preferences.ui:16
msgid "Check Spelling While Typing"
msgstr "Memeriksa Ejaan Saat Mengetik"

#: data/ui/Preferences.ui:28
msgid "Auto-Hide Header Bar"
msgstr "Sembunyi Otomatis Bilah Tajuk"

#: data/ui/Preferences.ui:29
msgid "Auto-hide header and status bars while typing"
msgstr "Sembunyi otomatis bilah tajuk dan bilah status saat mengetik"

#: data/ui/Preferences.ui:42
msgid "Large Text"
msgstr "Teks Besar"

#: data/ui/Preferences.ui:43
msgid "Reduce the margins width and increase the text size when possible"
msgstr "Kurangi lebar marjin dan perbesar ukuran teks ketika mungkin"

#: data/ui/Preferences.ui:56
msgid "Restore Session"
msgstr "Pulihkan Sesi"

#: data/ui/Preferences.ui:57
msgid "Return to your previous session when Apostrophe is started"
msgstr "Kembali ke sesi sebelumnya ketika Aposthrope dimulai"

#: data/ui/Preferences.ui:70
msgid "Input Format"
msgstr "Format Masukan"

#: data/ui/Preferences.ui:71
msgid "Flavor of markdown Apostrophe will use"
msgstr "Varian markdown yang akan dipakai oleh Aposthrope"

#: data/ui/Preferences.ui:80
msgid "Markdown Flavor Documentation"
msgstr "Dokumentasi Varian Markdown"

#: data/ui/PreviewLayoutSwitcher.ui:7 apostrophe/preview_window.py:30
msgid "Preview"
msgstr "Pratinjau"

#: data/ui/PreviewLayoutSwitcher.ui:24
msgid "Screen Layout"
msgstr "Tata Letak Layar"

#: data/ui/Recents.ui:46
msgid "No Recent Documents"
msgstr "Tidak Ada Dokumen Terkini"

#: data/ui/Recents.ui:69
msgid "No Results Found"
msgstr "Tidak Ditemukan Hasil"

#: data/ui/SearchBar.ui:29
msgid "Previous Match"
msgstr "Cocok Sebelumnya"

#: data/ui/SearchBar.ui:38
msgid "Next Match"
msgstr "Cocok Berikutnya"

#. Translators: This indicates case sensitivity, so it should be the first vowel of the language in lowercase and uppercase
#: data/ui/SearchBar.ui:53
msgid "aA"
msgstr "aA"

#: data/ui/SearchBar.ui:56
msgid "Case Sensitive"
msgstr "Peka Huruf Besar Kecil"

#: data/ui/SearchBar.ui:65
msgid "Regular Expression"
msgstr "Ekspresi Reguler"

#: data/ui/SearchBar.ui:73 data/ui/SearchBar.ui:111
msgid "Replace"
msgstr "Ganti"

#: data/ui/SearchBar.ui:119
msgid "Replace All"
msgstr "Ganti Semua"

#: data/ui/Shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Umum"

#: data/ui/Shortcuts.ui:14
msgctxt "shortcut window"
msgid "New"
msgstr "Baru"

#: data/ui/Shortcuts.ui:20
msgctxt "shortcut window"
msgid "Open"
msgstr "Buka"

#: data/ui/Shortcuts.ui:26
msgctxt "shortcut window"
msgid "Save"
msgstr "Simpan"

#: data/ui/Shortcuts.ui:32
msgctxt "shortcut window"
msgid "Save As"
msgstr "Simpan Sebagai"

#: data/ui/Shortcuts.ui:38
msgctxt "shortcut window"
msgid "Close Document"
msgstr "Tutup Dokumen"

#: data/ui/Shortcuts.ui:44
msgctxt "shortcut window"
msgid "Show Preferences"
msgstr "Tampilkan Preferensi"

#: data/ui/Shortcuts.ui:50
msgctxt "shortcut window"
msgid "Keyboard Shortcuts"
msgstr "Pintasan Papan Tik"

#: data/ui/Shortcuts.ui:56
msgctxt "shortcut window"
msgid "Quit Application"
msgstr "Keluar Aplikasi"

#: data/ui/Shortcuts.ui:64
msgctxt "shortcut window"
msgid "Modes"
msgstr "Mode"

#: data/ui/Shortcuts.ui:67
msgctxt "shortcut window"
msgid "Focus Mode"
msgstr "Mode Fokus"

#: data/ui/Shortcuts.ui:73
msgctxt "shortcut window"
msgid "Hemingway Mode"
msgstr "Mode Hemingway"

#: data/ui/Shortcuts.ui:80
msgctxt "shortcut window"
msgid "Preview"
msgstr "Pratinjau"

#: data/ui/Shortcuts.ui:86
msgctxt "shortcut window"
msgid "Fullscreen"
msgstr "Layar Penuh"

#: data/ui/Shortcuts.ui:94 data/ui/Shortcuts.ui:97
msgctxt "shortcut window"
msgid "Find"
msgstr "Cari"

#: data/ui/Shortcuts.ui:103
msgctxt "shortcut window"
msgid "Find and Replace"
msgstr "Cari dan Ganti"

#: data/ui/Shortcuts.ui:111
msgctxt "shortcut window"
msgid "Markdown"
msgstr "Markdown"

#: data/ui/Shortcuts.ui:114
msgctxt "shortcut window"
msgid "Separator"
msgstr "Pemisah"

#: data/ui/Shortcuts.ui:120
msgctxt "shortcut window"
msgid "Header"
msgstr "Tajuk"

#: data/ui/Shortcuts.ui:126
msgctxt "shortcut window"
msgid "List Item"
msgstr "Butir Daftar"

#: data/ui/Shortcuts.ui:133
msgctxt "shortcut window"
msgid "Italic"
msgstr "Miring"

#: data/ui/Shortcuts.ui:139
msgctxt "shortcut window"
msgid "Bold"
msgstr "Tebal"

#: data/ui/Shortcuts.ui:145
msgctxt "shortcut window"
msgid "Strikeout"
msgstr "Coret"

#: data/ui/Shortcuts.ui:153
msgctxt "shortcut window"
msgid "Copy and Paste"
msgstr "Salin dan Tempel"

#: data/ui/Shortcuts.ui:156
msgctxt "shortcut window"
msgid "Copy"
msgstr "Salin"

#: data/ui/Shortcuts.ui:162
msgctxt "shortcut window"
msgid "Cut"
msgstr "Potong"

#: data/ui/Shortcuts.ui:168
msgctxt "shortcut window"
msgid "Paste"
msgstr "Tempel"

#: data/ui/Shortcuts.ui:176
msgctxt "shortcut window"
msgid "Undo and Redo"
msgstr "Tak Jadi dan Jadi Lagi"

#: data/ui/Shortcuts.ui:179
msgctxt "shortcut window"
msgid "Undo Previous Command"
msgstr "Tak Jadi Jalankan Perintah Sebelumnya"

#: data/ui/Shortcuts.ui:185
msgctxt "shortcut window"
msgid "Redo Previous Command"
msgstr "Jadi Lagi Jalankan Perintah Sebelumnya"

#: data/ui/Shortcuts.ui:193
msgctxt "shortcut window"
msgid "Selection"
msgstr "Pilihan"

#: data/ui/Shortcuts.ui:196
msgctxt "shortcut window"
msgid "Select All Text"
msgstr "Pilih Semua Teks"

#: data/ui/Statsbar.ui:21 data/ui/Statsbar.ui:40
msgid "Show Statistics"
msgstr "Tampilkan Statistik"

#: data/ui/Statsbar.ui:22
msgid "0 Words"
msgstr "0 Kata"

#: data/ui/TexliveWarning.ui:25 data/ui/TexliveWarning.ui:79
msgid "TexLive Required"
msgstr "Diperlukan TexLive"

#: data/ui/TexliveWarning.ui:33
msgid ""
"Apostrophe needs the TeXLive extension\n"
"in order to export PDF or LaTeX files.\n"
"\n"
"Install it from Apostrophe's page in Software\n"
"or by running the following command in a terminal:"
msgstr ""
"Apostrof memerlukan ekstensi TeXLive\n"
"untuk mengekspor berkas PDF atau LaTeX.\n"
"\n"
"Pasang dari halaman Apostrof di Perangkat Lunak\n"
"atau dengan menjalankan perintah berikut di terminal:"

#: data/ui/TexliveWarning.ui:53
msgid "Copy to Clipboard"
msgstr "Salin ke Papan Klip"

#: data/ui/TexliveWarning.ui:87
msgid ""
"Apostrophe needs TeXLive\n"
"in order to export PDF or LaTeX files.\n"
"\n"
"Install it from your distribution repositories."
msgstr ""
"Apostrof membutuhkan TeXLive\n"
"untuk mengekspor berkas PDF atau LaTeX.\n"
"\n"
"Pasang dari repositori distribusi Anda."

#: data/ui/ThemeSwitcher.ui:17
msgid "Use System Colors"
msgstr "Gunakan Warna Sistem"

#: data/ui/ThemeSwitcher.ui:27
msgid "Use Light Colors"
msgstr "Gunakan Warna Terang"

#: data/ui/ThemeSwitcher.ui:37
msgid "Use Sepia Colors"
msgstr "Gunakan Warna Sepia"

#: data/ui/ThemeSwitcher.ui:47
msgid "Use Dark Colors"
msgstr "Gunakan Warna Gelap"

#: data/ui/Window.ui:81
msgid "File Has Changed on Disk"
msgstr "Berkas Telah Berubah pada Diska"

#: data/ui/Window.ui:91
msgid "The file has been changed by another program"
msgstr "Berkas telah diubah oleh program lain"

#: data/ui/Window.ui:99
msgid "_Discard Changes and Reload"
msgstr "Buang Peru_bahan dan Muat Ulang"

#: apostrophe/application.py:223
msgid "Donate"
msgstr "Donasi"

#: apostrophe/application.py:224
msgid "Translations"
msgstr "Terjemahan"

#: apostrophe/export_dialog.py:121 apostrophe/helpers.py:84
msgid "Close"
msgstr "Tutup"

#: apostrophe/export_dialog.py:129 apostrophe/export_dialog.py:346
#, python-format
msgid "Export to %s"
msgstr "Ekspor ke %s"

#: apostrophe/export_dialog.py:155 apostrophe/export_dialog.py:335
#, python-brace-format
msgid ""
"An error happened while trying to export:\n"
"\n"
"{err_msg}"
msgstr ""
"Kesalahan terjadi saat mencoba mengekspor:\n"
"\n"
"{err_msg}"

#: apostrophe/export_dialog.py:234
msgid "Export to {}"
msgstr "Ekspor ke {}"

#: apostrophe/export_dialog.py:342
msgid "Select folder"
msgstr "Pilih folder"

#: apostrophe/helpers.py:83
msgid "Error"
msgstr "Galat"

#: apostrophe/inhibitor.py:36
msgid "Unsaved documents"
msgstr "Dokumen yang belum disimpan"

#: apostrophe/inline_preview.py:201
msgid "Formula looks incorrect:"
msgstr "Rumus terlihat salah:"

#: apostrophe/inline_preview.py:241
msgid "No matching footnote found"
msgstr "Tidak ada catatan kaki yang cocok ditemukan"

#: apostrophe/inline_preview.py:261
msgid "noun"
msgstr "kata benda"

#: apostrophe/inline_preview.py:263
msgid "verb"
msgstr "kata kerja"

#: apostrophe/inline_preview.py:265
msgid "adjective"
msgstr "kata sifat"

#: apostrophe/inline_preview.py:267
msgid "adverb"
msgstr "kata keterangan"

#. Hemingway Toast
#: apostrophe/main_window.py:143
msgid "Text can't be deleted while on Hemingway mode"
msgstr "Teks tidak dapat dihapus saat berada di mode Hemingway"

#: apostrophe/main_window.py:146
msgid "Tell me more"
msgstr "Ceritakan lebih banyak"

#: apostrophe/main_window.py:435
msgid "Save your File"
msgstr "Simpan berkas Anda"

#: apostrophe/main_window.py:438 apostrophe/main_window.py:607
msgid "Save"
msgstr "Simpan"

#: apostrophe/main_window.py:502
msgid "Markdown Files"
msgstr "Berkas Markdown"

#: apostrophe/main_window.py:506
msgid "Plain Text Files"
msgstr "Berkas Teks Biasa"

#: apostrophe/main_window.py:509
msgid "Open a .md file"
msgstr "Buka berkas .md"

#: apostrophe/main_window.py:512
msgid "Open"
msgstr "Buka"

#: apostrophe/main_window.py:599
msgid "Save Changes?"
msgstr "Simpan Perubahan?"

#: apostrophe/main_window.py:600
#, python-format
msgid ""
"“%s” contains unsaved changes. If you don’t save, all your changes will be "
"permanently lost."
msgstr ""
"\"%s\" berisi perubahan yang belum disimpan. Jika Anda tidak menyimpan, "
"semua perubahan Anda akan hilang secara permanen."

#: apostrophe/main_window.py:606
msgid "Discard"
msgstr "Buang"

#: apostrophe/main_window.py:763 apostrophe/main_window.py:787
msgid "New File"
msgstr "Berkas Baru"

#: apostrophe/preview_layout_switcher.py:41
msgid "Full-Width"
msgstr "Lebar Penuh"

#: apostrophe/preview_layout_switcher.py:43
msgid "Half-Width"
msgstr "Setengah Lebar"

#: apostrophe/preview_layout_switcher.py:45
msgid "Half-Height"
msgstr "Setengah Tinggi"

#: apostrophe/preview_layout_switcher.py:47
msgid "Windowed"
msgstr "Berjendela"

#: apostrophe/stats_handler.py:122
msgid "{:n} of {:n} Characters"
msgstr "{:n} dari {:n} Karakter"

#: apostrophe/stats_handler.py:124
msgid "{:n} Character"
msgid_plural "{:n} Characters"
msgstr[0] "{:n} Karakter"

#: apostrophe/stats_handler.py:127
msgid "{:n} of {:n} Words"
msgstr "{:n} dari {:n} Kata"

#: apostrophe/stats_handler.py:129
msgid "{:n} Word"
msgid_plural "{:n} Words"
msgstr[0] "{:n} Kata"

#: apostrophe/stats_handler.py:132
msgid "{:n} of {:n} Sentences"
msgstr "{:n} dari {:n}  Kalimat"

#: apostrophe/stats_handler.py:134
msgid "{:n} Sentence"
msgid_plural "{:n} Sentences"
msgstr[0] "{:n} Kalimat"

#: apostrophe/stats_handler.py:137
msgid "{:n} of {:n} Paragraphs"
msgstr "{:n} dari {:n} Paragraf"

#: apostrophe/stats_handler.py:139
msgid "{:n} Paragraph"
msgid_plural "{:n} Paragraphs"
msgstr[0] "{:n} Paragraf"

#: apostrophe/stats_handler.py:142
msgid "{:d}:{:02d}:{:02d} of {:d}:{:02d}:{:02d} Read Time"
msgstr "Waktu Baca {:d}:{:02d}:{:02d} dari {:d}:{:02d}:{:02d}"

#: apostrophe/stats_handler.py:144
msgid "{:d}:{:02d}:{:02d} Read Time"
msgstr "Waktu Baca {:d}:{:02d}:{:02d}"

#: apostrophe/text_view_format_inserter.py:24
msgid "italic text"
msgstr "teks miring"

#: apostrophe/text_view_format_inserter.py:29
msgid "bold text"
msgstr "teks tebal"

#: apostrophe/text_view_format_inserter.py:34
msgid "strikethrough text"
msgstr "teks coretan"

#: apostrophe/text_view_format_inserter.py:49
#: apostrophe/text_view_format_inserter.py:117
#: apostrophe/text_view_format_inserter.py:187
msgid "Item"
msgstr "Butir"

#: apostrophe/text_view_format_inserter.py:258
msgid "Header"
msgstr "Tajuk"

#: apostrophe/text_view_format_inserter.py:329
msgid "Quote"
msgstr "Kutip"

#: apostrophe/text_view_format_inserter.py:371
msgid "code"
msgstr "kode"

#: apostrophe/text_view_format_inserter.py:394
msgid "https://www.example.com"
msgstr "https://www.contoh.com"

#: apostrophe/text_view_format_inserter.py:395
msgid "link text"
msgstr "teks tautan"

#: apostrophe/text_view_format_inserter.py:429
#: apostrophe/text_view_format_inserter.py:461
msgid "image caption"
msgstr "kapsi gambar"

#: apostrophe/text_view_format_inserter.py:435
msgid "Image"
msgstr "Gambar"

#: apostrophe/text_view_format_inserter.py:441
msgid "Select an image"
msgstr "Pilih gambar"

#: apostrophe/text_view_format_inserter.py:550
msgid ""
"\n"
"code\n"
msgstr ""
"\n"
"kode\n"

#: apostrophe/text_view.py:161 apostrophe/text_view.py:163
msgid "web page"
msgstr "halaman web"

#~ msgid "Manuel G., Wolf V."
#~ msgstr "Manuel G., Wolf V."

#~ msgid "Autohide headerbar"
#~ msgstr "Sembunyi Otomatis Bilah Tajuk"

#~ msgid "Use bigger text"
#~ msgstr "Pakai teks yang lebih besar"

#~ msgctxt "shortcut window"
#~ msgid "Keyboard shortcuts"
#~ msgstr "Pintasan papan tik"

#~ msgctxt "shortcut window"
#~ msgid "Focus mode"
#~ msgstr "Mode fokus"

#~ msgctxt "shortcut window"
#~ msgid "Hemingway mode"
#~ msgstr "Mode Hemingway"

#~ msgctxt "shortcut window"
#~ msgid "Find and replace"
#~ msgstr "Cari dan ganti"

#~ msgctxt "shortcut window"
#~ msgid "Copy selected text to clipboard"
#~ msgstr "Salin teks yang dipilih ke papan klip"

#~ msgctxt "shortcut window"
#~ msgid "Cut selected text to clipboard"
#~ msgstr "Potong teks yang dipilih ke papan klip"

#~ msgctxt "shortcut window"
#~ msgid "Paste selected text from clipboard"
#~ msgstr "Tempel teks yang dipilih dari papan klip"

#~ msgid "Menu"
#~ msgstr "Menu"

#~ msgid "Apostrophe website"
#~ msgstr "Situs web Apostrophe"

#~ msgid "Paypal"
#~ msgstr "Paypal"

#~ msgid "Help to translate:"
#~ msgstr "Bantu untuk menerjemahkan:"

#~ msgid "Damned Lies"
#~ msgstr "Damned Lies"

#~ msgid "CSS File"
#~ msgstr "Berkas CSS"

#~ msgid "Save as…"
#~ msgstr "Simpan sebagai…"

#~ msgid "New Window"
#~ msgstr "Jendela Baru"

#~ msgid "Save changes to document “%s” before closing?"
#~ msgstr "Simpan perubahan ke dokumen \"%s\" sebelum menutup?"

#~ msgid "Close without saving"
#~ msgstr "Tutup tanpa menyimpan"

#~ msgid "Save now"
#~ msgstr "Simpan sekarang"
